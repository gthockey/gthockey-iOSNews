//
//  GTHockeyNewsExampleApp.swift
//  GTHockeyNewsExample
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI
import GTHockeyNews

@main
struct GTHockeyNewsExampleApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                NewsList(from: "https://gthockey.com/api/articles/").background(Color.background)
            }
        }
    }
}

//
//  ContentView.swift
//  GTHockeyNewsExample
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

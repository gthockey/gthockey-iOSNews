//
//  NewsList.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI
import GTHockeyUtils
import SDWebImageSwiftUI

public struct NewsList: View {
    
    @State private var articles: [Article] = []
    @State private var selectedArticle: Article?
    
    private let layout = [GridItem(.flexible())]
    
    private let endpoint: String
    
    public init(from endpoint: String) {
        self.endpoint = endpoint
    }
    
    public var body: some View {
        ScrollView {
            LazyVGrid(columns: layout, spacing: 20) {
                ForEach(articles) { article in
                    CustomGridCell(horizontalAlignment: .leading) {
                        HStack(alignment: .top, spacing: 10) {
                            WebImage(url: article.imageURL)
                                .resizable()
                                .indicator(.activity)
                                .transition(.fade)
                                .scaledToFill()
                                .frame(width: 120, height: 100)
                                .clipped()
                                .cornerRadius(5)
                            VStack(alignment: .leading, spacing: 5) {
                                CustomText(article.title, font: .normalTextSemibold)
                                    .lineLimit(3)
                                CustomText(article.date.dateFromDayOnly.mediumDateOnly, font: .smallCaption)
                            }
                        }
                    }
                    .padding(.horizontal, 20)
                    .onTapGesture {
                        selectedArticle = article
                    }
                }
            }
        }
        .onAppear { fetchArticles() }
        .fullScreenCover(item: $selectedArticle) { article in
            ArticleView(article: article)
        }
        .navigationTitle("News")
    }
    
    private func fetchArticles() {
        NewsManager.shared.getArticles(from: endpoint, completion: { result in
            switch result {
            case .success(let articles):
                self.articles = articles
            case .failure(let error):
                // TODO: Present an error to the user
                print(error.localizedDescription)
            }
        })
    }
}

//struct NewsList_Previews: PreviewProvider {
//    static var previews: some View {
//        NewsList()
//    }
//}


extension String {
    
    /// A `Date` representation of a string
    var dateFromDayOnly: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: self)!
    }
    
}

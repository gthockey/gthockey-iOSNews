//
//  ArticleView.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI
import SDWebImageSwiftUI
import GTHockeyUtils

internal struct ArticleView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    internal var article: Article
    
    @State private var bodyContentHeight: CGFloat = 0
    
    internal var body: some View {
        ZStack(alignment: .topTrailing) {
            ScrollView {
                GeometryReader { geo in
                    WebImage(url: article.imageURL)
                        .resizable()
                        .indicator(.activity)
                        .transition(.fade)
                        .scaledToFill()
                        .frame(width: geo.size.width)
                }
                .frame(height: 350)
                CustomText(article.title, font: .heading3)
                    .lineLimit(3)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 5)
                    .padding(.horizontal, 15)
                CustomText(article.date.dateFromDayOnly.mediumDateOnly, font: .standardCaption)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 20)
                    .padding(.horizontal, 15)
                HTMLText(article.content, font: .lightText, height: $bodyContentHeight)
                    .frame(height: bodyContentHeight)
                    .padding(.bottom, 30)
                    .padding(.horizontal, 15)
            }
            .edgesIgnoringSafeArea(.top)
            
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle.fill")
            }
            .font(.heading3)
            .background(Color.navy)
            .clipShape(Circle())
            .foregroundColor(.powder)
            .padding([.top, .trailing])
        }
        .background(Color.background.edgesIgnoringSafeArea(.vertical))
    }
}

//struct ArticleView_Previews: PreviewProvider {
//    static var previews: some View {
//        ArticleView()
//    }
//}

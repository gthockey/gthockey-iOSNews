//
//  TestModels.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import Foundation

internal struct NewsTestModels {
    
    // MARK: Test Article Objects
    
    static let article1 = Article(id: 121,
                                  title: "Fall Recap - Jackets get ready for Nationals Push",
                                  date: "2022-01-04",
                                  imageURL: URL(string: "https://prod.gthockey.com/media/news/POster_Spring_2022.jpg")!,
                                  teaser: "After a long wait, your Yellow Jackets returned for a full slate of games in the fall 2021 semester.",
                                  content:
    """
After a long wait, your Yellow Jackets returned for a full slate of games in the fall 2021 semester.
The Yellow Jackets got off to a hot starting beating Clemson and the current 2nd ranked team in the nation Tampa, handing them their only loss thus far. After the hot start, the Yellow Jackets went on a series of road trips to Charleston and Huntsville, where the boys took care of business. The Yellow Jackets then headed over to Classic Center in Athens, GA, where, for the first time since 2014, took down down their in-state rivals, UGA Bulldogs, in a 4-3 thriller.

November saw the Jackets travel to Macon, GA, to participate in the first Annual Macon Veterans Cup! While the tournament did not go exactly as planned for the Yellow Jackets, they were able to secure their second win against UGA in the semester (A 2-1 record against the Dawgs this season for those counting at home). A special thanks to the Macon Collesium for hosting the great event!

The season was not without adversity, however, as the Yellow Jackets has to fight through several key injuries and absences. However, through strong leadership from returning players such as Matt Connelly, Brandon Holt, and Min Kim, and major contributions from newcomers such as Ryyan Reule, Zach Jacobson, and Earl Gretzinger (the CHF November freshman of the month) the Yellow Jackets were able to persevere.
Overall, the Yellow Jackets assembled a 12-5 record overall, and a 5-0 SECHC record, putting them at 2nd in the conference.

The excitement did not just stop on the ice though! The Jackets made waves across the hockey community with their gorgeous new gold jerseys designed by the team President (and unofficial graphic designer), Min Kim. Inspired by the logos worn by the Yellow Jackets in the early 2000s, the Retro Gold Jerseys made its debut in November with GT thrashing Kennesaw State University.

With the fall semester wrapped up nicely in a bow, your Jackets look to rally for a strong push towards the organization's return to the National Championships since 2018.

After a one-year hiatus, the ever-important Savannah Hockey Classic is back! After opening up the Spring semester with a home doubleheader against the University of Florida, the Yellow Jackets will travel to Savannah 1/14-1/15 to take on FSU and UGA, hoping to once again hoist the Thrasher Cup. Head over to <a href="https://www.savannahhockeyclassic.com/"> the Savannah Hockey Classic Website</a> to grab your tickets and get more information.

Tech will also host the Annual ACC-South Challenge Tournament starting February the 11th with the University of Clemson and Florida State University coming to town. The SECHC Regional Tournament will follow soon after, as the Jackets will compete with some of the South East’s best hockey programs in order to claim the SECHC championship, and punch a ticket to nationals.

For a full schedule of games, as well as times and locations, please visit <a href="https://www.gthockey.com/schedule"> the Schedule tab</a> and if you have any further questions or concerns please feel free to contact us<a href = "mailto: mink.gthockey@gmail.com"> here</a>!
""")
    
}

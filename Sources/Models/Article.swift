//
//  Article.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import Foundation

/// An object to represent a single news article
internal struct Article: Identifiable, Decodable {
    
    let id: Int
    let title: String
    let date: String
    let imageURL: URL
    let teaser: String
    let content: String
    
    enum CodingKeys: String, CodingKey {
        case imageURL = "image"
        case id, title, date, teaser, content
    }
    
}

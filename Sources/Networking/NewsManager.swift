//
//  NewsManager.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import Foundation
import GTHockeyUtils

/// The networking layer class for fetching a news feed
internal class NewsManager {
    
    internal static var shared = NewsManager()
    
    /// Fetches list of news articles with a given url endpoint
    /// - Parameters:
    ///   - urlString: a `String` that represents the endpoint to reach out to
    ///   - completion: a completion type of either `.success([Article])` or `.failure(NetworkingError)` indicating
    ///   the results of the fetch
    internal func getArticles(from urlString: String, completion: @escaping (Result<[Article], NetworkingError>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidUrl))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            
            do {
                let games = try JSONDecoder().decode([Article].self, from: data)
                completion(.success(games))
            } catch {
                completion(.failure(.parsingError))
            }
        }
        task.resume()
    }
    
}

